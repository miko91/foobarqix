module.exports = {
  processNumber: (number) => {
    const divisors = {
      3: 'Foo',
      5: 'Bar',
      7: 'Qix',
      11: 'Piou'
    };

    var chain = '';

    chain += Object.keys(divisors)
      .map((d) => number % d === 0 ? divisors[d] : '')
      .join('');

    const numberAsString = number.toString();
    const numberLen = numberAsString.length;

    for (let i = 0; i < numberLen; i++) {
      for (let divisor in divisors) {
        if ((i + divisor.length) <= numberLen && numberAsString.substring(i, i + divisor.length) === divisor) {
          chain += divisors[divisor];
        }
      }
    }

    return chain.length === 0 ? number.toString() : chain;
  }
};
