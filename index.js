const fooBarQix = require('./foobarquix');

console.log(Array(100).fill().map((_, i) => i + 1).map((i) => fooBarQix.processNumber(i)).join('\n'));
