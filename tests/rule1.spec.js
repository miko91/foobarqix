'use strict'

const foobarquix = require('../foobarquix');
const expect = require('chai').expect

describe('if the number is divisible by 3 or contains 3, replace 3 by "Foo"', () => {
  it('3 should return "FooFoo"', () => {
    expect(foobarquix.processNumber(3)).to.equal('FooFoo');
  });
  it('9 should return "Foo"', () => {
    expect(foobarquix.processNumber(9)).to.equal('Foo');
  });
  it('13 should return "Foo"', () => {
    expect(foobarquix.processNumber(13)).to.equal('Foo');
  });
  it('33 should return "FooPiouFooFoo"', () => {
    expect(foobarquix.processNumber(33)).to.equal('FooPiouFooFoo');
  });
});

describe('if the number is divisible by 5 or contains 5, replace 5 by "Bar"', () => {
  it('5 should return "BarBar"', () => {
    expect(foobarquix.processNumber(5)).to.equal('BarBar');
  });
  it('10 should return "Bar"', () => {
    expect(foobarquix.processNumber(10)).to.equal('Bar');
  });
  it('15 should return "FooBarBar"', () => {
    expect(foobarquix.processNumber(15)).to.equal('FooBarBar');
  });
});

describe('if the number is devisible by 7 or contains 7, replace 7 by "Qix"', () => {
  it('7 should return "QixQix"', () => {
    expect(foobarquix.processNumber(7)).to.equal('QixQix');
  });
  it('14 should return "Qix"', () => {
    expect(foobarquix.processNumber(14)).to.equal('Qix');
  });
  it('77 should return "QixPiouQixQix"', () => {
    expect(foobarquix.processNumber(77)).to.equal('QixPiouQixQix');
  });
});

describe('divisors have high precedence', () => {
  it('51 should return "FooBar"', () => {
    expect(foobarquix.processNumber(51)).to.equal('FooBar');
  });
  it('72 should return "FooQix"', () => {
    expect(foobarquix.processNumber(72)).to.equal('FooQix');
  });
  it('35 should return "BarQixFooBar"', () => {
    expect(foobarquix.processNumber(35)).to.equal('BarQixFooBar');
  });
});

describe('the content is analysed in the order they appear', () => {
  it('53 should return "BarFoo"', () => {
    expect(foobarquix.processNumber(53)).to.equal('BarFoo');
  });
  it('73 should return "QixFoo"', () => {
    expect(foobarquix.processNumber(73)).to.equal('QixFoo');
  });
  it('751 should return "QixBar"', () => {
    expect(foobarquix.processNumber(751)).to.equal('QixBar');
  });
});

describe('if the number is not in the FooBarQuix divisors, return the number', () => {
  it('1 should return "1"', () => {
    expect(foobarquix.processNumber(1)).to.equal('1');
  });
  it('2 should return "2"', () => {
    expect(foobarquix.processNumber(2)).to.equal('2');
  });
  it('4 should return "4"', () => {
    expect(foobarquix.processNumber(4)).to.equal('4');
  });
  it('8 should return "8"', () => {
    expect(foobarquix.processNumber(8)).to.equal('8');
  });
});

describe('if the number is devisible by 11 or contains 11, replace 11 by "Piou"', () => {
  it('11 should return "PiouPiou"', () => {
    expect(foobarquix.processNumber(11)).to.equal('PiouPiou');
  });
  it('55 should return "BarPiouBarBar"', () => {
    expect(foobarquix.processNumber(55)).to.equal('BarPiouBarBar');
  });
  it('111 should return "FooPiouPiou"', () => {
    expect(foobarquix.processNumber(111)).to.equal('FooPiouPiou');
  });
  it('1111 should return "PiouPiouPiouPiou"', () => {
    expect(foobarquix.processNumber(1111)).to.equal('PiouPiouPiouPiou');
  });
});
